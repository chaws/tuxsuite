# Devices

The following devices are currently supported by TuxTest:

* qemu-arm
* qemu-arm64
* qemu-i386
* qemu-mips64
* qemu-ppc64
* qemu-riscv64
* qemu-x86_64

In order to be compatible with build architectures, the folowing aliases are also recognised:

* qemu-mips: qemu-mips64
* qemu-powerpc: qemu-ppc64
* qemu-riscv: qemu-riscv64
